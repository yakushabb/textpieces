// SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

using Gtk 4.0;
using Adw 1;

template $TextPiecesActionSearch : Adw.Bin {
    child: Stack stack {
        styles ["view"]

        transition-type: crossfade;

        StackPage {
            name: "search";

            child: ScrolledWindow {
                child: Viewport viewport {
                    child: Adw.Clamp {

                        child: ListBox list_box {
                            styles ["boxed-list"]

                            vexpand: false;
                            valign: start;
                            margin-start: 10;
                            margin-end: 10;
                            margin-top: 10;
                            margin-bottom: 10;

                            selection-mode: browse;

                            row-activated => $on_row_activated() swapped;
                        };
                    };
                };
            };
        }

        StackPage {
            name: "not-found";

            child: Adw.StatusPage {
                icon-name: "applications-utilities-symbolic";
                title: C_("action search", "No Actions Found");

                child: Button {
                    styles ["pill"]

                    halign: center;
                    action-name: "win.custom-actions";

                    Adw.ButtonContent {
                        icon-name: "list-add-symbolic";

                        use-underline: true;
                        label: C_("action search", "_Create custom action");
                    }
                };
            };
        }
    };
}

EventControllerKey key_controller {
    propagation-phase: capture;

    key-pressed => $on_key_pressed() swapped;
}
