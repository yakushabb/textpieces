<!--
SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

# TODO

## Short-term plans

- Add JWT encoding/decoding actions
- Add decimal/hexadecimal/octal number conversion actions
- Add more hash function actions
- Add actions for TOMl
- Add case-operating actions

## Long-term plans

- Set up automatic changelogs and releases from convertial commits
- Start using Weblate
- Create WebAssembly-based actions support
- Create a repository for third-party actions
