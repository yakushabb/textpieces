<!--
SPDX-FileCopyrightText: 2021 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<div align="center">
<img
    src="data/icons/io.gitlab.liferooter.TextPieces.svg" alt="Text Pieces"
    width="128"
    height="128"/>
<h1> Text Pieces </h1>
Swiss knife of text processing
</div>

![Screenshot of Text Pieces](./data/screenshots/screenshot-1.png)

<br/>

# What is it?

Text Pieces is a small tool for quick text transformations such as checksums, encoding, decoding and so on.
You can use wide set of built-in actions, or easily write your own.

# Features

- Base64 encoding and decoding
- SHA-1, SHA-2 and MD5 checksums
- Prettify and minify JSON
- Convert JSON to YAML and vice versa
- Count lines, symbols and words
- Escape and unescape string, URL and HTML
- Remove leading and trailing whitespaces
- Sort and reverse sort lines
- Reverse lines and whole text
- Extendable with third-party scripts and custom tools

# Installation

## From Flathub

> **Recommended**

[Click here](https://flathub.org/apps/details/io.gitlab.liferooter.TextPieces) to install app from Flathub.

## Build from source

### Via GNOME Builder

Text Pieces can be built with GNOME Builder. Clone this repo and click run button.

### Via Flatpak

Text Pieces has a Flatpak manifest to enable it to be [built with Flatpak](https://docs.flatpak.org/en/latest/building-introduction.html).

### Via Meson

Text Pieces can be built directly via Meson:

```bash
git clone https://github.com/liferooter/textpieces
cd textpieces
meson _build
cd _build
meson compile
```

Next, it can be installed by `meson install`.

### Via Nix

*With flakes:* `nix install gitlab:liferooter/textpieces`

*Without flakes:* this repo also provides `default.nix` compatible with pre-flake Nix which evaluates into the package.

# Dependencies

If you use GNOME Builder or Flatpak, dependencies will be installed automatically. If you use pure Meson, dependencies will be:
- Not-so-old Rust compiler
- gtk >= 4.14
- gtksourceview >= 5.12
- glib >= 2.80
- libadwaita >= 1.5
